# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2018-11-07 11:24+0100\n"
"PO-Revision-Date: 2018-05-12 15:56+0200\n"
"Last-Translator: Tails translators\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Acknowledgements and similar projects\"]]\n"
msgstr "[[!meta title=\"Danksagungen und ähnliche Projekte\"]]\n"

#. type: Title =
#, no-wrap
msgid "Acknowledgements\n"
msgstr "Danksagungen\n"

#. type: Bullet: '  - '
msgid ""
"Tails could not exist without [[Debian|https://www.debian.org/]], [[Debian "
"Live|https://www.debian.org/devel/debian-live/]], and [[Tor|https://www."
"torproject.org/]]; see our [[contribute/relationship with upstream]] "
"document for details."
msgstr ""
"Tails wäre ohne [[Debian|https://www.debian.org/index.de.html]], [[Debian "
"Live|https://www.debian.org/devel/debian-live/]], und [[Tor|https://www."
"torproject.org/]] nicht möglich; siehe unsere [[Beziehungen zum Upstream|"
"contribute/relationship_with_upstream]] für Details."

#. type: Bullet: '  - '
msgid ""
"Tails was inspired by the [[Incognito LiveCD|http://web.archive.org/"
"web/20090220133020/http://anonymityanywhere.com/]]. The Incognito author "
"declared it to be dead on March 23rd, 2010, and wrote that Tails \"should be "
"considered as its spiritual successor\"."
msgstr ""
"Tails wurde durch die [[Incognito LiveCD|http://web.archive.org/"
"web/20090220133020/http://anonymityanywhere.com/]] inspiriert. Der Inkognito-"
"Autor erklärte diese am 23. März 2010 für tot und schrieb, dass Tails \"als "
"der geistige Nachfolger angesehen werden sollte\"."

#. type: Bullet: '  - '
msgid ""
"The [[Privatix Live-System|http://mandalka.name/privatix/]] was an early "
"source of inspiration, too."
msgstr ""
"Das [[Privatix Live-System|http://mandalka.name/privatix/]] war ebenfalls "
"eine frühe Quelle der Inspiration."

#. type: Bullet: '  - '
msgid ""
"Some ideas (in particular [[tordate|contribute/design/Time_syncing]] and "
"improvements to our [[contribute/design/memory_erasure]] procedure) were "
"borrowed from [Liberté Linux](http://dee.su/liberte)."
msgstr ""
"Einige Ideen (insbesondere [[tordate|contribute/design/Time_syncing]] und "
"die Verbesserung unserer [[Prozedur zum Löschen des Hauptspeichers|"
"contribute/design/memory_erasure]]) wurden von [Liberté Linux](http://dee.su/"
"liberte) entliehen."

#. type: Plain text
#, no-wrap
msgid "<a id=\"similar_projects\"></a>\n"
msgstr "<a id=\"similar_projects\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Similar projects\n"
msgstr "Ähnliche Projekte\n"

#. type: Plain text
msgid ""
"Feel free to contact us if you think that your project is missing, or if "
"some project is listed in the wrong category."
msgstr ""
"Wenn Sie glauben, dass Ihr Projekt hier fehlt oder ein Projekt in der "
"falschen Kategorie aufgeführt ist, dann kontaktieren Sie uns bitte."

#. type: Title ##
#, no-wrap
msgid "Active projects"
msgstr "Aktive Projekte"

#. type: Plain text
#, no-wrap
msgid "<!-- We degrade projects to 'discontinued' if they don't release a new version in at least 1 year. -->\n"
msgstr ""

#. type: Bullet: '* '
msgid "[Heads](https://heads.dyne.org/)"
msgstr "[Heads](https://heads.dyne.org/)"

#. type: Bullet: '* '
msgid "[Qubes](https://www.qubes-os.org/)"
msgstr "[Qubes](https://www.qubes-os.org/)"

#. type: Bullet: '* '
msgid ""
"[Trusted End Node Security](http://www.spi.dod.mil/lipose.htm) (previously "
"*Lightweight Portable Security*)"
msgstr ""
"[Trusted End Node Security](http://www.spi.dod.mil/lipose.htm) (früher "
"*Lightweight Portable Security*)"

#. type: Bullet: '* '
msgid "[Whonix](https://www.whonix.org/)"
msgstr "[Whonix](https://www.whonix.org/)"

#. type: Title ##
#, no-wrap
msgid "Discontinued, abandoned or sleeping projects"
msgstr "Eingestellte, aufgegebene und ruhende Projekte"

#. type: Bullet: '* '
msgid "[Anonym.OS](http://sourceforge.net/projects/anonym-os/)"
msgstr "[Anonym.OS](http://sourceforge.net/projects/anonym-os/)"

#. type: Bullet: '* '
msgid "[ELE](http://www.northernsecurity.net/download/ele/) (dead link)"
msgstr "[ELE](http://www.northernsecurity.net/download/ele/) (toter Link)"

#. type: Bullet: '* '
msgid ""
"[Estrella Roja](http://distrowatch.com/table.php?distribution=estrellaroja)"
msgstr ""
"[Estrella Roja](http://distrowatch.com/table.php?distribution=estrellaroja)"

#. type: Bullet: '* '
msgid "[Freepto](http://www.freepto.mx/)"
msgstr "[Freepto](http://www.freepto.mx/)"

#. type: Bullet: '* '
msgid "[IprediaOS](http://www.ipredia.org/)"
msgstr "[IprediaOS](http://www.ipredia.org/)"

#. type: Bullet: '* '
msgid "[ISXUbuntu](http://www.isoc-ny.org/wiki/ISXubuntu)"
msgstr "[ISXUbuntu](http://www.isoc-ny.org/wiki/ISXubuntu)"

#. type: Bullet: '* '
msgid ""
"[JonDo Live-CD](https://anonymous-proxy-servers.net/en/jondo-live-cd.html)"
msgstr "[JonDo Live-CD](https://www.anonym-surfen.de/jondo-live-cd.html)"

#. type: Bullet: '* '
msgid "[Liberté Linux](http://dee.su/liberte)"
msgstr "[Liberté Linux](http://dee.su/liberte)"

#. type: Bullet: '* '
msgid "[Odebian](http://www.odebian.org/)"
msgstr "[Odebian](http://www.odebian.org/)"

#. type: Bullet: '* '
msgid "[onionOS](http://jamon.name/files/onionOS/) (dead link)"
msgstr "[onionOS](http://jamon.name/files/onionOS/) (toter Link)"

#. type: Bullet: '* '
msgid "[ParanoidLinux](http://www.paranoidlinux.org/) (dead link)"
msgstr "[ParanoidLinux](http://www.paranoidlinux.org/) (toter Link)"

#. type: Bullet: '* '
msgid "[Phantomix](http://phantomix.ytternhagen.de/)"
msgstr "[Phantomix](http://phantomix.ytternhagen.de/)"

#. type: Bullet: '* '
msgid "[Polippix](http://polippix.org/)"
msgstr "[Polippix](http://polippix.org/)"

#. type: Bullet: '* '
msgid "[Privatix](http://www.mandalka.name/privatix/)"
msgstr "[Privatix](http://www.mandalka.name/privatix/)"

#. type: Bullet: '* '
msgid "[SubgraphOS](https://subgraph.com/sgos/)"
msgstr "[SubgraphOS](https://subgraph.com/sgos/)"

#. type: Bullet: '* '
msgid "[The Haven Project](https://www.haven-project.org/) (dead link)"
msgstr "[The Haven Project](https://www.haven-project.org/) (toter Link)"

#. type: Bullet: '* '
msgid ""
"[The Incognito LiveCD](http://web.archive.org/web/20090220133020/http://"
"anonymityanywhere.com/)"
msgstr ""
"[The Incognito LiveCD](http://web.archive.org/web/20090220133020/http://"
"anonymityanywhere.com/)"

#. type: Bullet: '* '
msgid "[Ubuntu Privacy Remix](https://www.privacy-cd.org/)"
msgstr "[Ubuntu Privacy Remix](https://www.privacy-cd.org/)"

#. type: Bullet: '* '
msgid "[uVirtus](http://uvirtus.org/)"
msgstr "[uVirtus](http://uvirtus.org/)"

#~ msgid "[Lightweight Portable Security](http://www.spi.dod.mil/lipose.htm)"
#~ msgstr "[Lightweight Portable Security](http://www.spi.dod.mil/lipose.htm)"
